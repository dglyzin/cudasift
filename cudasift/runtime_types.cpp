#include "runtime_types.h"
#include "sift.h"
#include "data_flow.h"
#include "image_scale.h"
#include "gaussian_filter.h"
#include "extrema_detection.h"
#include "orientation_detection.h"
#include "misc.h"

#include "sift_keypoint.h"

#include <stdio.h>
#include <iostream>
#include <assert.h>

void sift_image_group::blur() {

	std::cout << "Blurring" << std::endl;

	samples[0] = gaussian_blur(origin, sigmas[0]);

	for(int i = 1; i < MAX_SCALES; i ++) {

		samples[i] = gaussian_blur(samples[i-1], sigmas[i]);
	}

	/*for(int i = 0; i < MAX_SCALES; i ++) {

		samples[i] = gaussian_blur(origin, sigmas[i]);
	}*/

	std::cout << "Blurred!" << std::endl;
}

void sift_image_group::dog() {

	std::cout << "Differing" << std::endl;

	for(int i = 0; i < MAX_SCALES - 1; i ++) {

		differences[i]= difference_of_gaussian(samples[i + 1], samples[i]);
	}

	std::cout << "Differed!" << std::endl;
}

void sift_image_group::extract_extrema() {

	std::cout << "Extrema extraction" << std::endl;

	for(int i = 0; i < MAX_SCALES - 3; i ++) {

		extrema[i] = detect_extrema(differences[i],	differences[i + 1],	differences[i + 2]);
	}
}

void sift_image_group::assign_orientation() {

	for(int i = 0; i < MAX_SCALES - 3; i ++) {

		orientation[i] = detect_orientation(samples[i + 1]);

		magnitude[i] = detect_magnitude(samples[i + 1]);

		smoothed_magnitude[i] = gaussian_blur(samples[i + 1], sigmas[i + 1] * 1.5f);
	}
}

void sift_image_group::init() {

	//std::cout << "Initialising group" << std::endl;

	sigmas = (float *)malloc(sizeof(float) * MAX_SCALES);

	samples = (float_image **)malloc(sizeof(float_image *) * MAX_SCALES);

	differences = (float_image **)malloc(sizeof(float_image *) * (MAX_SCALES - 1));

	extrema = (float_image **)malloc(sizeof(float_image *) * (MAX_SCALES - 3));

	orientation = (float_image **)malloc(sizeof(float_image *) * (MAX_SCALES - 3));

	magnitude = (float_image **)malloc(sizeof(float_image *) * (MAX_SCALES - 3));

	smoothed_magnitude = (float_image **)malloc(sizeof(float_image *) * (MAX_SCALES - 3));

	//std::cout << "Initialised : samples **" << std::endl;

	for(int i = 0; i < MAX_SCALES; i ++) {

		sigmas[i] = get_sigma(i);

		//std::cout << "Sigma [" << i << "] = " << sigmas[i] << std::endl;
	}

	//std::cout << "Initialised : differences * []" << std::endl;

	//std::cout << "Initialised!" << std::endl;

	blur();

	dog();

	extract_extrema();

	//_sleep(600);

	std::cout << "Passed!" << std::endl;

	assign_orientation();
}

void sift_image_group::extract_keypoints(int idx) {

	vector<abstract_keypoint> points;

	_sleep(600);

	std::cout << "Keypoints extraction ! " << std::endl;

	unsigned int scale = (unsigned int)pow(2.0, (double)index);
	
	unsigned int w = samples[idx]->width;
	unsigned int h = samples[idx]->height;

	//_sleep(600);

	//std::cout << "Keypoints extraction : part 1 " << std::endl;

	float * h_extrema = allocate_and_copy_host(extrema[idx]->data, extrema[idx]->size());

	//_sleep(600);

	//std::cout << "Keypoints extraction : part 2 " << std::endl;
	float * h_orientation = allocate_and_copy_host(orientation[idx]->data, orientation[idx]->size());

	//_sleep(600);

	//std::cout << "Keypoints extraction : part 3 " << std::endl;
	float * h_magnitude = allocate_and_copy_host(smoothed_magnitude[idx]->data, smoothed_magnitude[idx]->size());
	//_sleep(600);

	//std::cout << "Keypoints extraction : part 4 " << std::endl;

	// Get the kernel size for the Guassian blur
	int radius = get_kernel_radius(sigmas[idx + 1] * 1.5f);

	float histogram[BINS];

	// Iterate through all points at this octave and interval
	for(int xi = 0; xi < w; xi++) {
		for(int yi = 0; yi < h; yi++) {

			// We're at a keypoint
			if(!h_extrema[xi + yi * w]) continue;
				
			// Reset the histogram thingy
			for(int i = 0; i < BINS; i ++) histogram[i] = 0.0f;

			// Go through all pixels in the window around the extrema
			for(int k_x = (-1) * radius; k_x <= radius; k_x++) {
				for(int k_y = (-1) * radius; k_y <= radius; k_y ++) {
							
					// Ensure we're within the image
					if(xi + k_x < 0 || xi + k_x >= w || yi + k_y < 0 || yi + k_y >= h) continue;

					double ori_scalar = h_orientation[xi + k_x + (yi + k_y) * w];

					//if(sampleOrient <=-M_PI || sampleOrient>M_PI)
							//printf("Bad Orientation: %f\n", sampleOrient);
								
					ori_scalar += 3.14f;

					// Convert to degrees
					unsigned int ori_degrees = ori_scalar * 180 / 3.14f;
							
					histogram[(int)ori_degrees / (360/BINS)] += h_magnitude[xi + k_x + (yi + k_y) * w];
	
				}
			}

			// We've computed the histogram. Now check for the maximum
			float max_peak = histogram[0];

			unsigned int max_peak_index = 0;
			
			for(int i = 1; i < BINS; i ++) {
							
				if(histogram[i] > max_peak) {
								
					max_peak = histogram[i];
					
					max_peak_index = i;
				}
			}

			// List of magnitudes and orientations at the current extrema
			vector<float> ori;
			vector<float> mag;
			
			for(int k = 0; k < BINS; k ++) {
							
				// Do we have a good peak?
				if(histogram[k] > 0.8f * max_peak) {
								
					// Three points. (x2,y2) is the peak and (x1,y1)
					// and (x3,y3) are the neigbours to the left and right.
					// If the peak occurs at the extreme left, the "left
					// neighbour" is equal to the right most. Similarly for
					// the other case (peak is rightmost)
					
					float x1 = k - 1;
					float y1;
					float x2 = k;
					float y2 = histogram[k];
					float x3 = k+1;
					float y3;

					if(k == 0) {
									
						y1 = histogram[BINS - 1];
						y3 = histogram[1];
					}
					else if(k == BINS - 1) {

						y1 = histogram[BINS - 1];
						y3 = histogram[0];
					}
					else {

						y1 = histogram[k - 1];
						y3 = histogram[k + 1];
					}


					float * b = (float *)malloc(sizeof(float) * 3);

					matrix_3x3 * matrix = matrix_3x3::create();

					matrix->M[0][0] = x1 * x1;
					matrix->M[0][1] = x1;
					matrix->M[0][2] = 1;

					matrix->M[1][0] = x2 * x2;
					matrix->M[1][1] = x2;
					matrix->M[1][2] = 1;

					matrix->M[2][0] = x3 * x3;
					matrix->M[2][1] = x3;
					matrix->M[2][2] = 1;

					matrix_3x3 * m_inv = matrix->invert();

					b[0] = m_inv->M[0][0] * y1 + m_inv->M[0][1] * y2 + m_inv->M[0][2] * y3; 
					b[1] = m_inv->M[1][0] * y1 + m_inv->M[1][1] * y2 + m_inv->M[1][2] * y3;
					b[2] = m_inv->M[2][0] * y1 + m_inv->M[2][1] * y2 + m_inv->M[2][2] * y3; 

					float x0 = -b[1] / (2.f * b[0]);

					// Anomalous situation
					if(fabs(x0) > 2 * BINS)	x0 = x2;

					while(x0 < 0) x0 += BINS;

					while(x0 >= BINS) x0 -= BINS;

					// Normalize it
					float x0_n = x0*(2.f * 3.14f / (float)BINS);

					assert(x0_n >= 0.f && x0_n < 2.f * 3.14f);
					x0_n -= 3.14f;
					assert(x0_n>=-3.14f && x0_n<3.14f);

					ori.push_back(x0_n);
					mag.push_back(histogram[k]);
				}
			}

			//keypoint point;

			/*point.x = xi * scale / 2;
			point.y = yi * scale / 2;
			point.magnitude = mag;
			point.orientation = ori;
			point.scale = index * (MAX_SCALES - 3) + idx;*/

			points.push_back(abstract_keypoint(xi * scale / 2, yi * scale / 2, index * (MAX_SCALES - 3) + idx, mag, ori));
			
		}
	}	

	std::cout << "Total : " << points.size() << std::endl;
}

sift_image_group * sift_image_group::create(float_image * image, int idx) {

	sift_image_group * sample = (sift_image_group *)malloc(sizeof(sift_image_group));

	sample->index = idx;

	//sample->origin = gaussian_blur(image, PREBLUR_SIGMA);

	if(!sample->index) sample->origin = gaussian_blur(upsample(image), PREBLUR_SIGMA);

	else sample->origin = gaussian_blur(downsample(image), PREBLUR_SIGMA);

	//sample->origin = image;

	//sample->init();

	return sample;
}

bool sift_image_group::iterable() {

	if(origin->width % 2 || origin->height % 2) return false;

	if(index >= MAX_STEPS - 1) return false;

	return true;
}

sift_image_group * sift_image_group::iterate() {

	return sift_image_group::create(origin, index + 1);
}

void perform_sift(sift_image_group * target) {

	sift_image_group * ptr = target;

	ptr->next = NULL;

	ptr->init();

	ptr->extract_keypoints(0);

	while(ptr->iterable()) {

		ptr->next = ptr->iterate();

		ptr = ptr->next;

		ptr->init();

		ptr->extract_keypoints(0);

		ptr->next = NULL; 
	}
}