#ifndef _IMAGE_TYPES_H_
#define _IMAGE_TYPES_H_

class gpu_image;
class cpu_image;

class float_image {

public :

	static enum Location {Cpu, Gpu};

	Location location;

	unsigned int width, height, length;
	
	float * data;

	explicit float_image(unsigned int w, unsigned int h, Location l);

	size_t size();

};

class cpu_image : public float_image {

public :

	cpu_image(unsigned int w, unsigned int h);

	operator gpu_image   ();
	operator gpu_image * ();
	operator float *     ();

};

class gpu_image : public float_image {

public :

	gpu_image(unsigned int w, unsigned int h);

	operator cpu_image   ();
	operator cpu_image * ();
	operator float *     ();

};

char * make_dog_str(int octave, int scale);
char * make_scale_str(int octave, int scale);
char * make_extrema_str(int octave, int scale);

cpu_image * extract (char path[], int scale);

void export_image(cpu_image * image, char path[]);


#endif