#include "image_types.h"

/*! \file gaussian_filter.h
	\brief Gauss blurring and differing functions
 */

/*! \fn float get_sigma(int scale, int step)
	\brief Calculates sigma value
	\param scale Current scale value
	\param step Current processing step
	\see get_kernel_value()
 */
float get_sigma(int scale);

int get_kernel_radius(float sigma);

int get_kernel_width(int radius);

int get_kernel_size(int radius);

/*! \fn float get_kernel_value(int x, float sigma)
	\brief Calculates Gaussian kernel value for a line element <x>
	\param x Line element position
	\param sigma Sigma value
	\see get_kernel()
 */
float get_kernel_value(int x, float sigma);

/*! \fn float * get_kernel(float sigma)
	\brief Calculates 2D Gaussian kernel with radius KERNEL_RADIUS and \p sigma
	\param sigma Sigma value
	\return pointer for Gaussian kernel with size KERNEL_SIZE
	\see get_kernel_value()
 */
float * get_kernel(float sigma);

void print_kernel(float * kernel, int radius);

/* Cuda Math kernels wrappers */

extern "C" gpu_image * gaussian_blur(gpu_image * image, float sigma);

//-- Calculates Difference of Gaussian for two data samples and returns device pointer 
extern "C" gpu_image * difference_of_gaussian(gpu_image * minuend, gpu_image * subtr);
//--