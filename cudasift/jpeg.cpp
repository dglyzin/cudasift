#include <iostream>

#include "sift.h"
#include "image_types.h"
#include "runtime_types.h"
#include "data_flow.h"

extern "C" {

	#include <jpeglib.h>
}

cpu_image * extract (char path[], int scale) {

	FILE * source = fopen(path, "rb");

	if(!source) {

		printf("Cant locate data or open stream\n");
		return 0;
	}

	struct jpeg_decompress_struct jinfo;
	struct jpeg_error_mgr jerror;

	jinfo.err = jpeg_std_error(&jerror);
	
	jpeg_create_decompress(&jinfo);

	jpeg_stdio_src(&jinfo, source);

	jpeg_read_header(&jinfo, TRUE);

	jinfo.out_color_space = JCS_GRAYSCALE;

	jinfo.do_block_smoothing = false;

	jinfo.scale_num = scale;

	jpeg_start_decompress(&jinfo);

	int stride = jinfo.output_width * jinfo.out_color_components;

	cpu_image * image = new cpu_image(jinfo.output_width, jinfo.output_height);

	image->data = new float[image->width * image->height];

	JSAMPARRAY buffer = (JSAMPARRAY)malloc(sizeof(JSAMPROW));

	buffer[0] = (JSAMPROW)malloc(sizeof(JSAMPLE) * stride);

	unsigned int shift = 0;

	while(jinfo.output_scanline < jinfo.output_height) {

		jpeg_read_scanlines(&jinfo, buffer, 1);

		//memcpy(image->data + shift, buffer[0], stride);

		for(int i = 0; i < stride; i ++) image->data[shift + i] = (float)(buffer[0][i]) / 255.f;

		shift += stride;
	}

	jpeg_finish_decompress(&jinfo);

	jpeg_destroy_decompress(&jinfo);

	fclose(source);

	return image;
}

void export_image(cpu_image * image, char path[]) {

	std::cout << "Export file : " << path << std::endl;

	FILE * source = fopen(path, "wb");

	if(!source) {

		printf("Cant locate data or open stream\n");
		return;
	}

	JSAMPLE * buffer = (JSAMPLE *)malloc(sizeof(JSAMPLE) * image->width * image->height);

	//float * data = allocate_and_copy_host(image->data, image->size());

	for(int i = 0; i < image->width * image->height; i ++) buffer[i] = (unsigned char)(abs(image->data[i] * 255.f));

	struct jpeg_compress_struct jinfo;
	struct jpeg_error_mgr jerror;
	
	jpeg_create_compress(&jinfo);
	
	jpeg_stdio_dest(&jinfo, source);
	
	jinfo.err = jpeg_std_error(&jerror);

	std::cout << "Size : " << image->width << "x" << image->height << std::endl;
	
	jinfo.image_width = image->width;
	jinfo.image_height = image->height;
	jinfo.in_color_space = JCS_GRAYSCALE;
	jinfo.input_components = 1;
	
	jpeg_set_defaults(&jinfo);
	
	jpeg_start_compress(&jinfo, TRUE);

	JSAMPROW row_pointer[1];

	while (jinfo.next_scanline < jinfo.image_height) {

		row_pointer[0] = & buffer[jinfo.next_scanline * image->width];
	    
		jpeg_write_scanlines(&jinfo, row_pointer, 1);
	}

	jpeg_finish_compress(&jinfo);

	jpeg_destroy_compress(&jinfo);

	fclose(source);
}


char * make_scale_str(int step, int scale) {

	char * scale_path = "C:/Solutions/Cuda/cudasift/Out/scale";

	char name[20], * dest;

	dest = (char *)malloc(sizeof(char) * 80);

	sprintf(name, "/octave%i_scale%i", step, scale);

	strcpy(dest, scale_path);

	strcat(dest, name);

	strcat(dest, ".jpg");

	//printf(dest);

	return dest;
}

char * make_dog_str(int step, int dog) {

	char * dog_path = "C:/Solutions/Cuda/cudasift/Out/dog";

	char name[20], * dest;

	dest = (char *)malloc(sizeof(char) * 80);

	sprintf(name, "/octave%i_dog%i", step, dog);

	strcpy(dest, dog_path);

	strcat(dest, name);

	strcat(dest, ".jpg");

	//printf(dest);

	return dest;
}

char * make_extrema_str(int step, int extrema) {

	char * dog_path = "C:/Solutions/Cuda/cudasift/Out/extrema";

	char name[20], * dest;

	dest = (char *)malloc(sizeof(char) * 80);

	sprintf(name, "/octave%i_extrema%i", step, extrema);

	strcpy(dest, dog_path);

	strcat(dest, name);

	strcat(dest, ".jpg");

	//printf(dest);

	return dest;
}
