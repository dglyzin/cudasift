#include <malloc.h>

typedef struct uchar_image {

	int width;
	int height;

	unsigned int length;
	unsigned char * data;

	static uchar_image * empty_uchar_image() {

		return (uchar_image *)malloc(sizeof(uchar_image));
	}

	/*void print_info() {

		printf("Width  : %d\n", width);
		printf("Height : %d\n", height);
		printf("Stride : %d\n", stride);
		printf("Length : %d\n", length);
	}*/

};

typedef struct float_image {

	int width;
	int height;

	unsigned int length;
	
	float * data;

	static float_image * fromImage(uchar_image * image) {

		float_image * sample = (float_image *)malloc(sizeof(float_image));

		sample->data = (float *)malloc(sizeof(float) * image->length);

		for(int i = 0; i < image->length; i ++) {
			//NOTE : FIND A WAY CHEAPER
			sample->data[i] = (float)image->data[i];
		}

		sample->height = image->height;
		sample->width  = image->width;
		sample->length = image->length;

		return sample;
	}

};