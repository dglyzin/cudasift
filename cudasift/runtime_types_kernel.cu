#include <cuda.h>
#include <cuda_runtime.h>
#include <device_launch_parameters.h>
#include <device_functions.h>
//#include <thrust\device_vector.h>
#include "cpu_bitmap.h"

#include <iostream>
#include <iomanip>

#include "global_header.h"
#include "gaussian_filter.h"

#pragma comment(lib, "cudart")

__global__ void test_kernel(float * in, float * out) {

	int p = blockIdx.x;

	if(p < 127) out[p] = in[p] + in[p + 1];
	
	else out[p] = in[p] + 0.1;
}

void test() {

	float * a, * b, * a_dev, * b_dev;

	cudaMalloc((void **)&b_dev, sizeof(float) * 128);
	cudaMalloc((void **)&a_dev, sizeof(float) * 128);

	a = (float *)malloc(sizeof(float) * 128);
	b = (float *)malloc(sizeof(float) * 128);

	for(int i = 0; i < 128; i ++) a[i] = i * 5 - 0.1;

	cudaMemcpy(a_dev, a, sizeof(float) * 128, cudaMemcpyHostToDevice);

	test_kernel<<<128, 1>>>(a_dev, b_dev);

	cudaMemcpy(b, b_dev, sizeof(float) * 128, cudaMemcpyDeviceToHost);

	for(int i = 0; i < 128; i ++) {

		printf("%.6f -> %.6f", a[i], b[i]);
	}

}

void printF(ImageF * image) {

	float * data = allocate_and_copy_host(image->device_data, image->size());

	for(int i = 0; i < image->length; i ++) {

		std::cout << data[i] << std::endl;

		if(!(i % 128)) {

			std::cout << "End of sequence " << i / 128 << "!" << std::endl;
		}
	}
}

void compareF(ImageF * f, ImageF * s, int max, int step) {

	float * fdata = allocate_and_copy_host(f->device_data, f->size());
	float * sdata = allocate_and_copy_host(s->device_data, s->size());

	for(int i = 0; i < max; i += step) {

		printf("%.5f -> ", fdata[i]);
		printf("%.5f \n", sdata[i]);
	}
}

void show(Image * image) {

	CPUBitmap bitmap(image->width, image->height);

	for(int i = 0; i < image->length; i ++) {
		
		bitmap.get_ptr()[i * 4] = image->data[i];
		bitmap.get_ptr()[i * 4 + 1] = image->data[i];
		bitmap.get_ptr()[i * 4 + 2] = image->data[i];
		bitmap.get_ptr()[i * 4 + 3] = image->data[i];
	}

	bitmap.display_and_exit();
}

void showF(ImageF * image) {

	CPUBitmap bitmap(image->width, image->height);

	float * data = allocate_and_copy_host(image->device_data, image->size());

	for(int i = 0; i < image->length; i ++) {
		
		bitmap.get_ptr()[i * 4] = data[i];
		bitmap.get_ptr()[i * 4 + 1] = data[i];
		bitmap.get_ptr()[i * 4 + 2] = data[i];
		bitmap.get_ptr()[i * 4 + 3] = data[i];
	}

	bitmap.display_and_exit();
}


__global__ void downscale(float * in, float * out, int w, int h) {

	__shared__ float summary;

	if(!threadIdx.x && !threadIdx.y) summary = 0;

	__syncthreads();

	int x = blockIdx.x * 2 + threadIdx.x;
	int y = blockIdx.y * 2 * threadIdx.y;
	
	int id = y * w + x;

	atomicAdd(&summary, in[id] / (float)4);

	int out_id = blockIdx.y * (int)(w / 2) + blockIdx.x;

	__syncthreads();

	if(!threadIdx.x && !threadIdx.y) out[out_id] = summary;

}

/* Cuda kernel wrappers */

float * downscale(float * device_data, int w, int h) {
	
	float * scaled;

	cudaMalloc((void **)&scaled, sizeof(float) * w * h);

	downscale<<<0, 0>>>(device_data, scaled, w, h);

	return scaled;
}