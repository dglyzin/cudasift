#include <cuda.h>
#include <cuda_runtime.h>
#include <device_launch_parameters.h>

#include "image_types.h"
#include "data_flow.h"

/* Allocating and copying data between device and host */

float * allocate_and_copy_device(float * host_data, size_t size) {

	float * device_data;

	cudaMalloc((void **)&device_data, size);

	cudaMemcpy(device_data, host_data, size, cudaMemcpyHostToDevice);

	return device_data;
}

float_image * allocate_and_copy_device_image(float_image * host_data) {

	float * device_data;

	cudaMalloc((void **)&device_data, host_data->size());

	cudaMemcpy(device_data, host_data->data, host_data->size(), cudaMemcpyHostToDevice);

	float_image * image = new float_image(host_data->width, host_data->height, float_image::Gpu);

	image->data = device_data;

	return image;
}

float * allocate_and_copy_host(float * device_data, size_t size) {
	
	float * host_data;

	cudaHostAlloc((void **)&host_data, size, cudaHostAllocDefault);

	cudaMemcpy(host_data, device_data, size, cudaMemcpyDeviceToHost);

	return host_data;
}

float_image * allocate_and_copy_host_image(float_image * device_data) {
	
	float * host_data;

	cudaHostAlloc((void **)&host_data, device_data->size(), cudaHostAllocDefault);

	cudaMemcpy(host_data, device_data->data, device_data->size(), cudaMemcpyDeviceToHost);

	float_image * image = new float_image(device_data->width, device_data->height, float_image::Cpu);

	image->data = host_data;

	return image;
}

void delete_device(float * device_data) {
	
	cudaFree(device_data);
}