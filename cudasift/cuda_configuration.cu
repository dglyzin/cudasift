#include <cuda.h>
#include <cuda_runtime.h>
#include <device_launch_parameters.h>

#include "cuda_configuration.h"

#pragma comment(lib, "cudart")

cuda_launch_options get_configuration(int width, int height, cuda_operation_type type) {

	cuda_launch_options option;

	if(type == cuda_operation_type::Blur) {		

		/*option.threads = dim3(KERNEL_WIDTH, KERNEL_WIDTH);
		option.blocks  = dim3(width, height);*/

		return option;
	}

	if(type == cuda_operation_type::Differ) {

		cudaDeviceProp device_property;

		cudaGetDeviceProperties(&device_property, 0);

		option.threads = device_property.warpSize * 16;

		option.blocks = width * height / option.threads.x;

		return option;
	}

	if(type == cuda_operation_type::Extrema) {

		option.blocks  = dim3(width, height);
		option.threads = dim3(3, 3, 3);

		return option;
	}
}