#include <cuda.h>
#include <cuda_runtime.h>
#include <device_launch_parameters.h>

#include "extrema_detection.h"
#include "cuda_configuration.h"

#include <iostream>

#pragma comment (lib, "cudart")

cuda_launch_options get_extrema_kernel_configuration(int w, int h) {

	cuda_launch_options option;

	option.blocks  = dim3(w / 8, h / 8);
	option.threads = dim3(8, 8);

	return option;
}

/*__global__ void extrema_detection_kernel(float ** data, bool * extrema, int w, int h) {
//__global__ void extrema_detection_kernel(float * up, float * mid, float * bot, bool * extrema, int w, int h) {

	if((blockIdx.x < 1) || (blockIdx.x > w - 2)) return;
	if((blockIdx.y < 1) || (blockIdx.y > h - 2)) return;

	int t_x = blockIdx.x + threadIdx.x - 1;
	int t_y = blockIdx.y + threadIdx.y - 1;

	int t_idx = t_x + t_y * w;

	__shared__ int min_counter, max_counter;

	float value = mid[blockIdx.x + w * blockIdx.y];

	//__shared__ float values[3][3][3];

	__syncthreads();

	if(!threadIdx.x && !threadIdx.y && !threadIdx.z) {
		
		min_counter = 0;
		max_counter = 0;

	}

	/*if(!threadIdx.x && !threadIdx.y) {

		int x_start = blockIdx.x - 1, y_start = blockIdx.y - 1;
		
		for(int y = 0; y < 3; y ++) {			

			for(int x = 0; x < 3; x ++) {

				values[x][y][threadIdx.z] = data[threadIdx.z][x_start + x + w * (y_start + y)];
			}
		}
	}

	__syncthreads();

	if((threadIdx.x != 1) && (threadIdx.y != 1) && (threadIdx.z != 1)) {

		float * ptr = threadIdx.z == 0 ? up : (threadIdx.z == 1 ? mid : bot); 
		//float * ptr = mid;

		if(ptr[t_idx] < value) atomicAdd(&max_counter, 1);
		if(ptr[t_idx] > value) atomicAdd(&min_counter, 1);

		//if(values[threadIdx.x][threadIdx.y][threadIdx.z] < values[1][1][1]) atomicAdd(&max_counter, 1);
		//if(values[threadIdx.x][threadIdx.y][threadIdx.z] > values[1][1][1]) atomicAdd(&min_counter, 1);

	}

	__syncthreads();

	if(!threadIdx.x && !threadIdx.y && !threadIdx.z) {

		//if(!((max_counter > 16) || (min_counter > 16))) return;

		if((max_counter > 7)) {

		//if(min_counter + max_counter != 26) {
		
		/*if(fabs(values[1][1][1]) < PEAK_THRESHOLD) return;

		float dxx, dxy, dyy;

		dxx = values[1][0][1] + values[1][2][1] - values[1][1][1] * 2.0;

		dyy = values[0][1][1] + values[2][1][1] - values[1][1][1] * 2.0;

		dxy = (values[0][0][1] + values[2][2][1] - values[0][1][1] - values[2][0][1]) / 4.0;

		float detH, trH;

		trH = dxx + dyy;
		
		detH = dxx * dyy - dxy * dxy;

		float curvature_ratio = trH * trH / detH;

		float curvature_threshold = (EDGE_THRESHOLD + 1.0) * (EDGE_THRESHOLD + 1.0) / EDGE_THRESHOLD;

		if(detH < 0 || curvature_ratio > curvature_threshold) return;

			extrema[blockIdx.x + blockIdx.y * w] = true;
		}
	}

	//else return;
}*/

__device__ inline bool check_min(float ** data, int w, int idx) {

	if(data[0][idx - 1 - w] > data[1][idx] &&
	   data[0][idx - w	  ] > data[1][idx] &&
	   data[0][idx - w + 1] > data[1][idx] &&
	   
	   data[0][idx - 1] > data[1][idx] &&
	   data[0][idx	  ] > data[1][idx] &&
	   data[0][idx + 1] > data[1][idx] &&
	   
	   data[0][idx + w - 1] > data[1][idx] &&
	   data[0][idx + w	  ] > data[1][idx] &&
	   data[0][idx + w + 1] > data[1][idx] &&
	   
	   data[2][idx - 1 - w] > data[1][idx] &&
	   data[2][idx - w	  ] > data[1][idx] &&
	   data[2][idx - w + 1] > data[1][idx] &&
	   
	   data[2][idx - 1] > data[1][idx] &&
	   data[2][idx	  ] > data[1][idx] &&
	   data[2][idx + 1] > data[1][idx] &&
	   
	   data[2][idx + w - 1] > data[1][idx] &&
	   data[2][idx + w	  ] > data[1][idx] &&
	   data[2][idx + w + 1] > data[1][idx] &&
	   
	   data[1][idx - 1 - w] > data[1][idx] &&
	   data[1][idx - w	  ] > data[1][idx] &&
	   data[1][idx - w + 1] > data[1][idx] &&
	   
	   data[0][idx - 1] > data[1][idx] &&
	   data[0][idx + 1] > data[1][idx] &&
	   
	   data[0][idx + w - 1] > data[1][idx] &&
	   data[0][idx + w	  ] > data[1][idx] &&
	   data[0][idx + w + 1] > data[1][idx])

	   return true;

	else return false;
}

__device__ inline bool check_max(float ** data, int w, int idx) {

	if(data[0][idx - 1 - w] < data[1][idx] &&
	   data[0][idx - w	  ] < data[1][idx] &&
	   data[0][idx - w + 1] < data[1][idx] &&
	   
	   data[0][idx - 1] < data[1][idx] &&
	   data[0][idx	  ] < data[1][idx] &&
	   data[0][idx + 1] < data[1][idx] &&
	   
	   data[0][idx + w - 1] < data[1][idx] &&
	   data[0][idx + w	  ] < data[1][idx] &&
	   data[0][idx + w + 1] < data[1][idx] &&
	   
	   data[2][idx - 1 - w] < data[1][idx] &&
	   data[2][idx - w	  ] < data[1][idx] &&
	   data[2][idx - w + 1] < data[1][idx] &&
	   
	   data[2][idx - 1] < data[1][idx] &&
	   data[2][idx	  ] < data[1][idx] &&
	   data[2][idx + 1] < data[1][idx] &&
	   
	   data[2][idx + w - 1] < data[1][idx] &&
	   data[2][idx + w	  ] < data[1][idx] &&
	   data[2][idx + w + 1] < data[1][idx] &&
	   
	   data[1][idx - 1 - w] < data[1][idx] &&
	   data[1][idx - w	  ] < data[1][idx] &&
	   data[1][idx - w + 1] < data[1][idx] &&
	   
	   data[0][idx - 1] < data[1][idx] &&
	   data[0][idx + 1] < data[1][idx] &&
	   
	   data[0][idx + w - 1] < data[1][idx] &&
	   data[0][idx + w	  ] < data[1][idx] &&
	   data[0][idx + w + 1] < data[1][idx])

	   return true;

	else return false;
}

__global__ void extrema_detection_kernel(float ** data, float * extrema, int w, int h) {

	int t_x = blockIdx.x * blockDim.x + threadIdx.x;// - 1;
	int t_y = blockIdx.y * blockDim.y + threadIdx.y;// - 1;
	
	int idx = t_x + t_y * w;
	
	extrema[idx] = 0.f;

	if((t_x < 1) || (t_x > w - 2) || (t_y < 1) || (t_y > h - 2)) return;

	//

	if(fabs(data[1][idx]) < PEAK_THRESHOLD) return;

	float dxx, dxy, dyy;

	dxx = data[1][idx - w] + data[1][idx + 2] - data[1][idx] * 2.f;

	dyy = data[1][idx - 1] + data[1][idx + 1] - data[1][idx] * 2.f;

	dxy = (data[1][idx - w - 1] + data[1][idx + w + 1] - data[1][idx + w - 1] - data[1][idx - w + 1]) / 4.f;

	float detH, trH;

	trH = dxx + dyy;
		
	detH = dxx * dyy - dxy * dxy;

	float curvature_ratio = trH * trH / detH;

	if((detH < 0) || (curvature_ratio > CURVATURE_THRESHOLD)) return;

	if(check_max(data, w, idx) || check_min(data, w, idx)) extrema[idx] = 1.f;

}

bool checkBorders(float * data, int w, int h) {

	for(int i = 0; i < h; i ++) if(data[w * i] != 0) return false;
	for(int i = 0; i < h; i ++) if(data[w - 1 + w * i] != 0) return false;

	for(int i = 0; i < w; i ++) if(data[i] != 0) return false;
	for(int i = 0; i < w; i ++) if(data[i + (h - 1) * w] != 0) return false;

	return true;
}

gpu_image * detect_extrema(gpu_image * up, gpu_image * mid, gpu_image * bot) {

	//std::cout << "CUDA extrema detection : " << cudaGetErrorString(cudaGetLastError()) << std::endl;

	int w = up->width, h = up->height;

	float ** data, ** dev_data;

	cudaMalloc((void **)&dev_data, sizeof(float *) * 3);

	//std::cout << "cudaMalloc [float device_ptr **] : " << cudaGetErrorString(cudaGetLastError()) << std::endl;

	cudaHostAlloc((void **)&data, sizeof(float *) * 3, cudaHostAllocDefault);

	//std::cout << "cudaHostAlloc [float host_ptr **] : " << cudaGetErrorString(cudaGetLastError()) << std::endl;

	data[0] = up-> data;
	data[1] = mid->data;
	data[2] = bot->data;

	cudaMemcpy(dev_data, data, sizeof(float *) * 3, cudaMemcpyHostToDevice);

	//std::cout << "cudaMemcpy[host -> float device_ptr **] : " << cudaGetErrorString(cudaGetLastError()) << std::endl;

	cuda_launch_options op = get_extrema_kernel_configuration(w, h);

	//std::cout << "Launch : " << op.blocks.x << "x" << op.blocks.y << " -> " << op.threads.x << "x" << op.threads.y << "x" << op.threads.z << std::endl;

	float * extrema, * dev_extrema;

	cudaMalloc((void **)&dev_extrema, sizeof(float) * w * h);

	//std::cout << "cudaMalloc [bool device_ptr *] : " << cudaGetErrorString(cudaGetLastError()) << std::endl;

	extrema = (float *)malloc(sizeof(float) * w * h);	

	//for(unsigned int i = 0; i < w * h; i ++) extrema[i] = 0.f;

	//cudaMemcpy(dev_extrema, extrema, sizeof(float) * w * h, cudaMemcpyHostToDevice);

	//if(checkBorders(extrema, w, h)) std::cout << "Borders is ok!" << std::endl;

	extrema_detection_kernel<<<op.blocks, op.threads>>>(dev_data, dev_extrema, w, h);

	//std::cout << "cuda kernel status : " << cudaGetErrorString(cudaDeviceSynchronize()) << std::endl;

	//std::cout << "cuda kernel exec : " << cudaGetErrorString(cudaGetLastError()) << std::endl;

	cudaMemcpy(extrema, dev_extrema, sizeof(float) * w * h, cudaMemcpyDeviceToHost); 

	//std::cout << "cudaMemcpy [bool device_ptr * -> host] : " << cudaGetErrorString(cudaGetLastError()) << std::endl;

	int c = 0;

	//if(checkBorders(extrema, w, h)) std::cout << "Borders is ok!" << std::endl;


	for(int i = 0; i < w; i ++)
		for(int j = 0; j < h; j ++) {

			if(extrema[i + j * w] == 1.f) c ++;
		}

	std::cout << "Found extrema -> " << c << std::endl << std::endl;

	gpu_image * image = new gpu_image(w, h);

	image->data = dev_extrema;

	return image;
}

